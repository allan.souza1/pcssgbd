CREATE DATABASE IF NOT EXISTS helpnity_dev;

CREATE TABLE IF NOT EXISTS usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    senha VARCHAR(255) NOT NULL,
    cpf VARCHAR(14) UNIQUE NOT NULL,
    data_nasc DATE,
    telefone VARCHAR(14),
    endereco VARCHAR(60),
    cidade VARCHAR(50),
    estado VARCHAR(30),
    competencias VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS ci_sessions (
    id varchar(128) NOT NULL,
    ip_address varchar(45) NOT NULL,
    timestamp int(10) unsigned DEFAULT 0 NOT NULL,
    data blob NOT NULL,
    KEY ci_sessions_timestamp (timestamp)
);

CREATE TABLE IF NOT EXISTS ordens_servico(
    id INT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(50) NOT NULL,
    categoria VARCHAR(50) NOT NULL,
    descricao VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS ordem_servico_usuarios(
    ordem_servico_id INT NOT NULL,
    usuario_solicitante_id INT NOT NULL,
    usuario_ajudante_id INT NOT NULL,
    CONSTRAINT PK_OrdemServico_USUARIOS PRIMARY KEY (ordem_servico_id, usuario_solicitante_id, usuario_ajudante_id),
    CONSTRAINT FK_Ordem_Servico FOREIGN KEY ordem_servico_id REFERENCES ordens_servico(id),
    CONSTRAINT FK_Usuario_Solicitante FOREIGN KEY usuario_solicitante_id REFERENCES usuario(id),
    CONSTRAINT FK_Usuario_Ajudante FOREIGN KEY usuario_ajudante_id REFERENCES usuario(id)
);