$(document).ready(function(){
    $('#formulario').on('keyup keypress', function(e){
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });

    $('#inputCompetencias').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $('#inputCpf').inputmask({
        mask: "999.999.999-99"
    });
    
    $('#inputTelefone').inputmask({
        mask: "(99)99999-9999"
    });
    
});