<header>
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center text-light">
                <h1 class="font-weight-light">Quer "pedir um help" ou "dar um help"?</h1>
                <p class="lead">Você pode fazer ambos! Cadastre-se ou faça login.</p>
                <button type="button" class="btn btn-success">Saiba mais</button>
            </div>
        </div>
    </div>
</header>