<div class="container my-5">
    <div class="row justify-content-center text-center">
        <div class="col">
            <a class="d-block" href="<?php echo base_url();?>">
                <i class="far fa-handshake fa-6x text-light"></i>
            </a>
            <h1 class="text-light">Helpnity</h1>
            <h2 class="text-light">Login</h2>
        </div>
    </div>
        
    <?php echo form_open('login/autenticar', 'class="formulario text-light my-5"'); ?>
        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-8 col-lg-6">
                <?php 
                    echo form_label('E-mail', 'inputEmail');
                    echo form_input(array(
                        'class' => 'form-control my-1',
                        'name' => 'input_email',
                        'id' => 'inputEmail',
                        'placeholder' => 'Insira seu e-mail',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_email'));
                    echo '</span>';
                ?>
            </div>
        </div>
        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-8 col-lg-6">
                <?php 
                    echo form_label('Senha', 'inputSenha');
                    echo form_password( array(
                        'class' => 'form-control my-1',
                        'name' => 'input_senha',
                        'id' => 'inputSenha',
                        'placeholder' => 'Insira sua senha',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_senha'));
                    echo '</span>';
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('error'));
                    echo '</span>';
                ?>
            </div>
        </div>              

        <div class="form-row justify-content-center">
            <?php
                echo form_submit( array(
                    'class' => 'btn btn-lg btn-success',
                    'name' => 'btn_submit',
                    'value' => 'Entrar'
                ));
            ?>
        </div>
    <?php echo form_close(); ?>

    <div class="row justify-content-center mb-3">
        <?php echo $google_login_link; ?>
    </div>

    <div class="row justify-content-center">
        <p class="text-light">Ainda não possui cadastro?</p>
    </div>

    <div class="row justify-content-center">
        <a href="<?php echo base_url('cadastro');?>">Clique aqui.</a>
    </div>
</div>