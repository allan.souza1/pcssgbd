<body class="background-grey">
<section class="mt-5">
    <div class="container mt-5">
        <div class="row px-3">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#criarOrdemServico">
                <i class="fas fa-plus-square mr-3"></i>Novo
            </button>

            <div class="modal fade" id="criarOrdemServico" tabindex="-1" role="dialog" aria-labelledby="criarOrdemServicoTitulo" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="criarOrdemServicoTitulo">Ordem de Serviço</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                        <?php echo form_open('dashboard/criarOrdemServico', 'class="formulario"'); ?>
                            <div class="form-row justify-content-center">
                                <div class="form-group col-9">
                                    <?php 
                                        echo form_label('Título', 'tituloOS');
                                        echo form_input( array(
                                            'class' => 'form-control my-1',
                                            'name' => 'titulo_os',
                                            'id' => 'tituloOS',
                                            'placeholder' => 'Insira um título',
                                            'required' => TRUE
                                        ));
                                    ?>
                                    <?php
                                        echo '<span class="text-danger">';
                                        echo html_escape($this->session->flashdata('titulo_os'));
                                        echo '</span>';
                                    ?>
                                </div>
                            </div>

                            <div class="form-row justify-content-center">
                                <div class="form-group col-9">
                                    <?php 
                                        echo form_label('Categoria', 'categoriaOS');
                                        echo form_input( array(
                                            'class' => 'form-control my-1',
                                            'name' => 'categoria_os',
                                            'id' => 'categoriaOS',
                                            'placeholder' => 'Escolha a categoria',
                                            'required' => TRUE
                                        ));
                                    ?>
                                    <?php
                                        echo '<span class="text-danger">';
                                        echo html_escape($this->session->flashdata('categoria_os'));
                                        echo '</span>';
                                    ?>
                                </div>
                            </div>

                            <div class="form-row justify-content-center">
                                <div class="form-group col-9">
                                    <?php 
                                        echo form_label('Descrição', 'descricaoOS');
                                        echo form_textarea( array(
                                            'class' => 'form-control my-1',
                                            'name' => 'descricao_os',
                                            'id' => 'descricaoOS',
                                            'placeholder' => 'Descreva o caso',
                                            'required' => TRUE
                                        ));
                                    ?>
                                    <?php
                                        echo '<span class="text-danger">';
                                        echo html_escape($this->session->flashdata('descricao_os'));
                                        echo '</span>';
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <?php
                                echo form_submit( array(
                                    'class' => 'btn btn-success',
                                    'id' => 'btnSubmitOS',
                                    'name' => 'btn_submit',
                                    'value' => 'Enviar'
                                ));
                            ?>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>        
    </div>

    <div class="container my-5">
        <div class="row">
            <div class="card-deck" id="lista_ordens_servico"></div>
        </div>
    </div>
</section>