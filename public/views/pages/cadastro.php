<div class="container my-5">
    <div class="row justify-content-center text-center">
        <div class="col">
            <a class="d-block" href="<?php echo base_url();?>">
                <i class="far fa-handshake fa-6x text-light"></i>
            </a>
            <h1 class="text-light">Helpnity</h1>
            <h2 class="text-light">Formulário de cadastro</h2>
        </div>
    </div>

    <?php echo form_open('cadastro/cadastrar', 'class="formulario text-light my-5"'); ?>
        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Nome', 'inputNome');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_nome',
                        'id' => 'inputNome',
                        'placeholder' => 'Insira seu nome completo',
                        'required' => TRUE                        
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_nome'));
                    echo '</span>';
                ?>                
            </div>

            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('CPF', 'inputCpf');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_cpf',
                        'id' => 'inputCpf',
                        'maxlength' => '14',
                        'placeholder' => 'Insira seu CPF',
                        'required' => TRUE       
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_cpf'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Data de nascimento', 'inputDataNasc');
                    echo form_input( array(
                        'type' => 'date',
                        'class' => 'form-control',
                        'name' => 'input_data_nasc',
                        'id' => 'inputDataNasc',
                        'placeholder' => 'dd/mm/aaaa',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_data_nasc'));
                    echo '</span>';
                ?>
            </div>

            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Cidade', 'inputCidade');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_cidade',
                        'id' => 'inputCidade',
                        'placeholder' => 'Insira sua cidade',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_cidade'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Estado', 'inputEstado');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_estado',
                        'id' => 'inputEstado',
                        'placeholder' => 'Insira seu estado',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_estado'));
                    echo '</span>';
                ?>
            </div>

            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Endereço', 'inputEndereco');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_endereco',
                        'id' => 'inputEndereco',
                        'placeholder' => 'Insira seu endereço',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_endereco'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Telefone', 'inputTelefone');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_telefone',
                        'id' => 'inputTelefone',
                        'maxlength' => '14',
                        'placeholder' => 'Insira seu telefone',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_telefone'));
                    echo '</span>';
                ?>
            </div>
            <div class="form-group col-10 col-md-5 col-lg-4">
                <?php 
                    echo form_label('Competências', 'inputCompetencias');
                    echo form_input( array(
                        'name' => 'input_competencias',
                        'id' => 'inputCompetencias',
                        'placeholder' => 'Insira suas competências',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_competencias'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
            <h3 class="text-light">Informações de login</h3>
        </div>

        <div class="form-row justify-content-center">                    
            <div class="form-group col-10 col-md-8 col-lg-6">
                <?php 
                    echo form_label('E-mail', 'inputEmail');
                    echo form_input( array(
                        'class' => 'form-control',
                        'name' => 'input_email',
                        'id' => 'inputEmail',
                        'placeholder' => 'Insira seu e-mail',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_email'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-8 col-lg-6">
                <?php 
                    echo form_label('Senha', 'inputSenha');
                    echo form_password( array(
                        'class' => 'form-control',
                        'name' => 'input_senha',
                        'id' => 'inputSenha',
                        'placeholder' => 'Crie uma senha',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_senha'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-8 col-lg-6">
                <?php 
                    echo form_label('Confirme sua senha', 'inputConfirmarSenha');
                    echo form_password( array(
                        'class' => 'form-control',
                        'name' => 'input_confirmar_senha',
                        'id' => 'inputConfirmarSenha',
                        'placeholder' => 'Repita sua senha',
                        'required' => TRUE
                    ));
                ?>
                <?php
                    echo '<span class="text-danger">';
                    echo html_escape($this->session->flashdata('input_confirmar_senha'));
                    echo '</span>';
                ?>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <div class="form-group col-10 col-md-12">
                <div class="form-check text-center">
                    <?php
                        echo form_checkbox( array(
                            'name' => 'checkbox_termos',
                            'class' => 'form-check-input',
                            'id' => 'checkboxTermos',
                            'required' => TRUE
                        ));
                
                        echo form_label('Estou ciente dos '.anchor('cadastro','Termos de uso'), 'checkboxTermos');
                    ?>
                    <?php
                        echo '<span class="d-block text-danger">';
                        echo html_escape($this->session->flashdata('checkbox_termos'));
                        echo '</span>';
                    ?>
                </div>
            </div>
        </div>

        <div class="form-row justify-content-center">
            <?php
                echo form_submit( array(
                    'class' => 'btn btn-lg btn-success',
                    'name' => 'btn_submit',
                    'value' => 'Cadastrar'
                ));
            ?>
        </div>              
    <?php echo form_close(); ?>

    <div class="row justify-content-center">
        <p class="text-light">Já possui cadastro?</p>
    </div>

    <div class="row justify-content-center">
        <?php 
            echo anchor('login', 'Faça login');
        ?>
    </div>
</div>

