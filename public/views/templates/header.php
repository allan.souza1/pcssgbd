<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php echo $titulo; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/all.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/selectize.default.css');?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favico.png');?>" sizes="100x100">
</head>

<?php 
    if ( current_url() == base_url('home') ) {
        echo '<body class="d-flex flex-column">';
    }

    else {
        echo '<body class="d-flex flex-column bckg-grey">';
    }
?>
