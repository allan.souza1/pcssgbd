<footer class="footer mt-auto py-3">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h5 class="text-light m-0 py-1"> Feito com <a class="text-success" href="https://getbootstrap.com/">Bootstrap</a> e <a class="text-success" href="https://fontawesome.com/">Font Awesome</a></h5>
            </div>
        </div>

        <div class="row">
            <div class="col text-center">
                <h5 class="text-light m-0 py-1">Imagens: <a class="text-success" href="https://unsplash.com/">Unsplash</a></h5>
            </div>
        </div>

        <div class="row">
            <div class="col text-center">
                <h5 class="text-light m-0 py-1">© 2019 Helpnity</h5>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-2.2.4.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.bundle.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/selectize.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.inputmask.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/script.js');?>"></script>

</body>

</html>