<div class="col-sm col-md-6">
    <div class="card">
        <div class="card-header"><?php echo html_escape($os_id); ?></div>
        <div class="card-body">
            <h5 class="card-title font-weight-bold"><?php echo html_escape($os_titulo); ?></h5>
            <h6 class="card-subtitle mb-2 text-muted"><?php echo html_escape($os_categoria); ?></h6>
        </div>
        <div class="card-body overflow-auto">
            <p class="card-text">
                <?php echo html_escape($os_descricao); ?>
            </p>
            <a href="#" class="btn btn-danger">Excluir</a>
            <a href="#" class="btn btn-primary">Editar</a>
        </div>
    </div>
</div>