<section class="mt-5">
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-4 mb-5 text-center">
                <img src="https://bootdey.com/img/Content/avatar/avatar6.png">
                <h3 class="text-center mt-3 text-light">Nome do usuário</h3>
            </div>

            <div class="col-md-7">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true">Informações pessoais</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="troca-senha-tab" href="#troca-senha" aria-controls="troca-senha" role="tab" data-toggle="tab" aria-selected="false">Alterar senha</a>
                    </li>
                </ul>

                <?php  echo form_open('dashboard/atualizarCadastro', 'class="formulario text-light my-3"'); ?>
                
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="home" aria-labelledby="home-tab">
                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Nome', 'inputNome');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_nome',
                                        'id' => 'inputNome',
                                        'placeholder' => 'Insira seu nome completo',
                                        'required' => TRUE                        
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_nome'));
                                    echo '</span>';
                                ?>                
                            </div>

                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('CPF', 'inputCpf');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_cpf',
                                        'id' => 'inputCpf',
                                        'maxlength' => '14',
                                        'placeholder' => 'Insira seu CPF',
                                        'required' => TRUE       
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_cpf'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Data de nascimento', 'inputDataNasc');
                                    echo form_input( array(
                                        'type' => 'date',
                                        'class' => 'form-control',
                                        'name' => 'input_data_nasc',
                                        'id' => 'inputDataNasc',
                                        'placeholder' => 'dd/mm/aaaa',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_data_nasc'));
                                    echo '</span>';
                                ?>
                            </div>

                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Cidade', 'inputCidade');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_cidade',
                                        'id' => 'inputCidade',
                                        'placeholder' => 'Insira sua cidade',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_cidade'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Estado', 'inputEstado');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_estado',
                                        'id' => 'inputEstado',
                                        'placeholder' => 'Insira seu estado',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_estado'));
                                    echo '</span>';
                                ?>
                            </div>

                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Endereço', 'inputEndereco');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_endereco',
                                        'id' => 'inputEndereco',
                                        'placeholder' => 'Insira seu endereço',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_endereco'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Telefone', 'inputTelefone');
                                    echo form_input( array(
                                        'class' => 'form-control',
                                        'name' => 'input_telefone',
                                        'id' => 'inputTelefone',
                                        'maxlength' => '14',
                                        'placeholder' => 'Insira seu telefone',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_telefone'));
                                    echo '</span>';
                                ?>
                            </div>
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Competências', 'inputCompetencias');
                                    echo form_input( array(
                                        'name' => 'input_competencias',
                                        'id' => 'inputCompetencias',
                                        'placeholder' => 'Insira suas competências',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_competencias'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="troca-senha" aria-labelledby="troca-senha-tab">
                        <div class="form-row justify-content-center">                    
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Senha atual', 'inputSenhaAtual');
                                    echo form_password( array(
                                        'class' => 'form-control',
                                        'name' => 'input_senha_atual',
                                        'id' => 'inputSenhaAtual',
                                        'placeholder' => 'Insira sua senha atual',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_senha_atual'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Nova senha', 'inputNovaSenha');
                                    echo form_password( array(
                                        'class' => 'form-control',
                                        'name' => 'input_nova_senha',
                                        'id' => 'inputNovaSenha',
                                        'placeholder' => 'Crie uma senha',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_nova_senha'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>

                        <div class="form-row justify-content-center">
                            <div class="form-group col-12 col-md-6">
                                <?php 
                                    echo form_label('Confirme sua senha', 'inputConfirmarSenha');
                                    echo form_password( array(
                                        'class' => 'form-control',
                                        'name' => 'input_confirmar_senha',
                                        'id' => 'inputConfirmarSenha',
                                        'placeholder' => 'Repita a nova senha',
                                        'required' => TRUE
                                    ));
                                ?>
                                <?php
                                    echo '<span class="text-danger">';
                                    echo html_escape($this->session->flashdata('input_confirmar_senha'));
                                    echo '</span>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row justify-content-center">
                    <?php
                        echo form_submit( array(
                            'class' => 'btn btn-lg btn-success',
                            'name' => 'btn_submit',
                            'value' => 'Atualizar'
                        ));
                    ?>
                </div>
                
                <?php echo form_close(); ?>
                
            </div>
        </div>
    </div>
</section>