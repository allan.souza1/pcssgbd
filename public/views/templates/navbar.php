<nav class="navbar navbar-expand-lg navbar-light bg-light-green shadow fixed-top">
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url() ?>">
            <i class="far fa-handshake align-middle fa-lg"></i>
            Helpnity
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapsed" aria-controls="navbarCollapsed" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div id="navbarCollapsed" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <?php
                    echo $navbar;
                ?>
            </ul>
        </div>
    </div>
</nav>