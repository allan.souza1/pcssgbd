<div class="container my-5">
    <div class="row justify-content-center text-center">
        <div class="col">
            <a class="d-block" href="<?php echo base_url();?>">
                <i class="far fa-handshake fa-6x text-light"></i>
            </a>
            <h1 class="text-light">Helpnity</h1>
        </div>
    </div>
</div>

<div class="container my-5">
    <div class="row justify-content-center text-center">
        <div class="col">
            <h2 class="text-light">
                <?php 
                    echo html_escape($msg);
                ?>
            </h2>                
        </div>
    </div>
</div>