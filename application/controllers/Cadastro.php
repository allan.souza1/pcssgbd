<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('cadastro_model');
    }

    public function index() {
        if ( $this->isLogged() ) {
			redirect(base_url('dashboard'));
        }

        $data['titulo'] = 'Helpnity - Cadastro';
		$this->load->view('templates/header', $data);
		$this->load->view('pages/cadastro');
		$this->load->view('templates/footer');
    }

    public function cadastrar() {
        $this->form_validation->set_rules('input_nome', 'Nome', 'required|trim|alpha');
        $this->form_validation->set_rules('input_cpf', 'CPF', 'required|trim|exact_length[14]|is_unique[usuarios.cpf]', array('exact_length' => 'O campo deve conter 11 números'));
        $this->form_validation->set_rules('input_data_nasc', 'Data de Nascimento', 'trim');
        $this->form_validation->set_rules('input_cidade', 'Cidade', 'trim|alpha');
        $this->form_validation->set_rules('input_estado', 'Estado', 'trim|alpha');
        $this->form_validation->set_rules('input_endereco', 'Endereço', 'trim');
        $this->form_validation->set_rules('input_telefone', 'Telefone', 'trim|exact_length[14]', array('exact_length' => 'O campo deve conter 11 números'));
        $this->form_validation->set_rules('input_competencias', 'Competências', 'trim');
        $this->form_validation->set_rules('input_email', 'E-mail', 'required|trim|valid_email|is_unique[usuarios.email]');
        $this->form_validation->set_rules('input_senha', 'Senha', 'required|trim');
        $this->form_validation->set_rules('input_confirmar_senha', 'Confirme sua senha', 'required|trim|matches[input_senha]');
        $this->form_validation->set_rules('checkbox_termos', '', 'msg', array('msg' => 'É necessário aceitar os Termos de Uso'));
        $this->form_validation->set_message('required', 'O campo não pode estar vazio');
        $this->form_validation->set_message('alpha', 'O campo pode conter somente letras');
        $this->form_validation->set_message('numeric', 'O campo pode conter somente números');
        $this->form_validation->set_message('valid_email', 'Insira um e-mail válido');
        $this->form_validation->set_message('is_unique', 'Este e-mail já está cadastrado');

        

        if ($this->form_validation->run()) {
            $passwd = password_hash($this->input->post('input_senha', TRUE), PASSWORD_BCRYPT);
            $data = array(
                'nome' => $this->input->post('input_nome', TRUE),
                'cpf' => $this->input->post('input_cpf', TRUE),
                'data_nasc' => $this->input->post('input_data_nasc', TRUE),
                'cidade' => $this->input->post('input_cidade', TRUE),
                'estado' => $this->input->post('input_estado', TRUE),
                'endereco' => $this->input->post('input_endereco', TRUE),
                'telefone' => $this->input->post('input_telefone', TRUE),
                'competencias' => $this->input->post('input_competencias', TRUE),
                'email' => $this->input->post('input_email', TRUE),
                'senha' => $passwd,
            );

            if ($this->cadastro_model->registrar($data)) {
                $msg['msg'] = 'Usuário cadastrado com sucesso!';
                $msg['titulo'] = 'Helpnity';
                $this->load->view('templates/header', $msg);
                $this->load->view('templates/msg_echo', $msg);
                $this->load->view('templates/footer');
            }
        }

        else {
            $error_array = $this->form_validation->error_array();
            $this->session->set_flashdata($error_array);
            redirect(base_url('cadastro'));
        }
    }

    
}