<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	public function __construct() {
		parent:: __construct();
	}

	public function index() {
		if ($this->isLogged()) {
            redirect(base_url('dashboard'));
		}
		
		$data['titulo'] = 'Helpnity';
		$this->load->view('templates/header', $data);
		$this->renderNavbar();
		$this->load->view('pages/home');
		$this->load->view('templates/footer');
	}
}
