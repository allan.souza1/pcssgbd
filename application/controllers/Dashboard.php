<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    public function __construct() {
        parent:: __construct();
        $this->load->library('form_validation');
        $this->load->model('ordemServico_model');
    }
    
    public function index() {
        if ( !$this->isLogged() ) {
			redirect(base_url('login'));
        }
        
        $data['titulo'] = 'Helpnity - Dashboard';
        $this->load->view('templates/header', $data);
        $this->renderNavbar();
        $this->load->view('pages/user/dashboard');
        $this->load->view('templates/user/footer');
    }

    public function listarOrdemServico() {
        $ordens_servico = $this->ordemServico_model->getOrdensServico();

        if ( isset($ordens_servico) ) {
            foreach($ordens_servico as $row) {
                $data['os_id'] = 'OS_00'.$row['id'];
                $data['os_titulo'] = $row['titulo'];
                $data['os_categoria'] = $row['categoria'];
                $data['os_descricao'] = $row['descricao'];
                $this->load->view('templates/user/ordem_servico', $data);
            }

            return;
        }

        return NULL;
    }

    public function criarOrdemServico() {
        $this->form_validation->set_rules('titulo_os', 'Título', 'required|trim');
        $this->form_validation->set_rules('categoria_os', 'Categoria', 'required|trim');
        $this->form_validation->set_rules('descricao_os', 'Descrição', 'required');
        $this->form_validation->set_message('required', 'O campo não pode estar vazio');

        $ordem_servico = array(
            'titulo' => $this->input->post('titulo_os', TRUE),
            'categoria' => $this->input->post('categoria_os', TRUE),
            'descricao' => $this->input->post('descricao_os', TRUE)
        );

        if ( $this->form_validation->run() ) {
            $this->ordemServico_model->registrarOrdemServico($ordem_servico);
        }

        else {
            $error_array = $this->form_validation->error_array();
            $this->session->set_flashdata($error_array);
        }

        redirect(base_url('dashboard'));
    }

    public function config() {
        $data['titulo'] = 'Helpnity - Dashboard - Configurações';
        $this->load->view('templates/header', $data);
        $this->renderNavbar();
        $this->load->view('templates/user/configuracoes');
        $this->load->view('templates/user/footer');
    }

    /*public function atualizarCadastro() {

    }*/

    public function logout() {
        session_unset();
        session_write_close();
        redirect(base_url('login'));
    }
}