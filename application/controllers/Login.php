<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    private $g_client;

    public function __construct() {
        parent:: __construct();
        $this->load->library('form_validation');
        $this->load->model('login_model');
        $this->g_client = new Google_Client();
        $this->g_client->setClientId("");
        $this->g_client->setClientSecret("");
        $this->g_client->setRedirectUri(base_url('login/googleAuth'));
        $this->g_client->addScope("profile");
    }

    public function index() {
        if ($this->isLogged()) {
            redirect(base_url('dashboard'));
        }
        
        $auth_url = $this->g_client->createAuthUrl();
        $g_data['google_login_link'] = anchor($auth_url, '<i class="fab fa-google mr-3"></i>Login com Google', 'class="btn btn-primary"');

        $data['titulo'] = 'Helpnity - Login';
		$this->load->view('templates/header', $data);
		$this->load->view('pages/login', $g_data);
        $this->load->view('templates/footer');     
    }

    public function autenticar() {
        $this->form_validation->set_rules('input_email', 'E-mail', 'required|trim|valid_email');
        $this->form_validation->set_rules('input_senha', 'Senha', 'required|trim');
        $this->form_validation->set_message('required', 'O campo não pode estar vazio');
        $this->form_validation->set_message('valid_email', 'Digite um e-mail válido');


        $email = $this->input->post('input_email', TRUE);
        $senha = $this->input->post('input_senha', TRUE);

        if ( $this->form_validation->run() ) {
            if( $this->login_model->verificarLogin($email, $senha) ) {
                $user = $this->login_model->getUsuario($email);
                if ( isset($user) ) {
                    $_SESSION["nome"] = $user["nome"];
                    $_SESSION["user_id"] = $user["id"];
                    $_SESSION["email"] = $email;
                    $_SESSION["logged_in"] = TRUE;
                    redirect(base_url('dashboard'));
                }
            }
            
            $msg['error'] = 'Usuário e/ou senha incorretos';
            $this->session->set_flashdata($msg);
            redirect(base_url('login')); 
        }

        $error_array = $this->form_validation->error_array();
        $this->session->set_flashdata($error_array);
        redirect(base_url('login'));
        
    }

    public function googleAuth() {
        if ( isset($_SESSION['access_token']) ) {
            try {
                $this->g_client->setAccessToken($_SESSION['access_token']);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        else if ( isset($_GET['code']) ) {
            $token = $this->g_client->fetchAccessTokenWithAuthCode($_GET['code']);
            $_SESSION['access_token'] = $token;
        }

        else {
            redirect(base_url('login'));
            return;
        }

        $o_auth = new Google_Service_Oauth2($this->g_client);
        $userData = $o_auth->userinfo_v2_me->get();
        
        $_SESSION['access_token'] = $this->g_client->getAccessToken();
        $_SESSION["nome"] = $userData["givenName"];
        $_SESSION['logged_in'] = TRUE;
        redirect(base_url('dashboard'));
        return;
    }


}
