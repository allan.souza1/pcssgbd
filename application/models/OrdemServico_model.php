<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrdemServico_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function getOrdensServico() {
        $query = $this->db->get('ordens_servico', '5');
        $result_array = $query->result_array();
        
        if ( !empty($result_array) ) {
            return $result_array;
        }

        else {
            return NULL;
        }
    }

    public function registrarOrdemServico($form = array()) {
        if (!empty($form)) {
            $this->db->insert('ordens_servico', $form);
            return true;
        }

        return false;
    }
}