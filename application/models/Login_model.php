<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

    public function verificarLogin($email, $passwd) {
        $this->db->select('email, senha')->where('email', $email);
        $query = $this->db->get('usuarios');
        $row_array = $query->row_array();

        if ( !empty($row_array) && password_verify($passwd, $row_array['senha']) ) {
            return true;
        }

        else {
            return false;
        }
    }

    public function getUsuario($email) {
        $this->db->select('id, nome')->where('email', $email);
        $query = $this->db->get('usuarios');
        $row_array = $query->row_array();

        if ( !empty($row_array) ) {
            return $row_array;
        }

        else {
            return NULL;
        }
    }

    public function verificarUsuarioGoogle($email) {
        
    }

}