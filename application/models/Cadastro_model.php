<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function registrar($form = array()) {
        if (!empty($form)) {
            $this->db->insert('usuarios', $form);
            return true;
        }
        
        return false;
    }


}