<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->createSessionAfterLogout();
    }

    protected function isLogged() {
        if ( isset($_SESSION["logged_in"]) && ( isset($_SESSION["user_id"]) || isset($_SESSION['access_token']) ) ) {
            if ( $_SESSION["logged_in"] === TRUE ) {
                return TRUE;
            }
        }

        return FALSE;
    }

    protected function createSessionAfterLogout() {
        if (session_status() == PHP_SESSION_NONE || !$this->isLogged()) {
            session_write_close();
            session_start();
            $_SESSION['access_token'] = NULL;
            $_SESSION['user_id'] = NULL;
			$_SESSION['nome'] = NULL;
            $_SESSION['logged_in'] = FALSE;
            return TRUE;
        }
        
        return FALSE;
    }

    protected function renderNavbar() {
        if ( $this->isLogged() ) {
            $username = html_escape($_SESSION["nome"]);

            $data['navbar'] = '
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                    Olá, '. strtok($username, ' ') .'
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="'. base_url('dashboard/config') .'">Configurações</a>
                    <a class="dropdown-item" href="'. base_url('dashboard/logout') .'">Sair</a>
                </div>
            </li>';
            $this->load->view('templates/navbar', $data);
            return;
        }

        $data['navbar'] = '
        <li class="nav-item">
            <a class="nav-link" href="'. base_url('cadastro') .'">Cadastre-se</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="'. base_url('login') .'">Login</a>
        </li>';

        $this->load->view('templates/navbar', $data);
        return;
    }
}